from datetime import date
from application.salary import calculate_salary
from application.db.people import get_employees


if __name__ == '__main__':
    today = date.today()
    print('Today', today.strftime('%d.%m.%Y'))
    if today.day % 2 == 0:
        calculate_salary()
    else:
        get_employees()
